package strategies;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by amen on 8/17/17.
 */
public class StdInStrategy implements IInputStrategy {

    private Scanner sc;
    public StdInStrategy() {
        sc = new Scanner(System.in);
    }

    @Override
    public int getInt() {
        System.out.println("Int -> ");
        int intInput = sc.nextInt();
        return intInput;
    }

    @Override
    public String getString() {
        System.out.println("String ->");
        String s = sc.next();
        return s;
    }

    @Override
    public double getDouble() {
        System.out.println("Double ->");
        double doubleInput = sc.nextDouble();
        return doubleInput;
    }
}
