package strategies;

import java.io.*;
import java.util.Scanner;

/**
 * Created by amen on 8/17/17.
 */
public class FileStrategy implements IInputStrategy {

    private File file;
    private Scanner sc;

    public FileStrategy() {
        try {
            this.file = new File("file.txt");
            sc = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("No file!");
        }
    }

    @Override
    public int getInt() {
        int intFile = 0;
        while (sc.hasNextLine()) {
            try {
                String line = sc.nextLine();
                intFile = Integer.parseInt(line);
                break;
            } catch (NumberFormatException e) {
                continue;
            }
        }
        return intFile;
    }

    @Override
    public String getString() {
        String stringFile = null;
        while (sc.hasNextLine()) {
            stringFile = sc.nextLine();
            break;
        }
        return stringFile;
    }


    @Override
    public double getDouble() {
        double doubleFile = 0.0;
        while (sc.hasNextLine()) {
            try {
                String line = sc.nextLine();
                doubleFile = Double.parseDouble(line);
                break;
            } catch (NumberFormatException e) {
                continue;
            }
        }
        return doubleFile;
    }
}
