package strategies;

import java.util.Random;

/**
 * Created by amen on 8/17/17.
 */
public class RandomStrategy implements IInputStrategy {
    @Override
    public int getInt() {
        Random random = new Random();
        int randomInt = random.nextInt();
        return randomInt;
    }

    @Override
    public String getString() {
        Random random = new Random();
        int dlugosc = random.nextInt(20);
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < dlugosc ; i++) {
            sb.append((char) (random.nextInt(32)+97));
        }
        return sb.toString();
    }

    @Override
    public double getDouble() {
        Random random = new Random();
        double randomDouble = random.nextDouble();
        return randomDouble;
    }
}
